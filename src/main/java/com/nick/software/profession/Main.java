package com.nick.software.profession;

import com.nick.software.profession.entity.Lawyer;
import com.nick.software.profession.entity.SoftwareEngineer;

import java.util.HashSet;

public class Main {
    public static void main(String[] args) {

        Lawyer lawyer1 = new Lawyer("test1", "test", "test", 11, null,
                "test", 11, 2,11 , new HashSet<String>());

        SoftwareEngineer softwareEngineer = new SoftwareEngineer("test2", "test",
                                                    "test", 111, lawyer1, "test",
                                                                null, null, null, null);

        softwareEngineer.entireExperience().forEach(s -> {
            if (s instanceof SoftwareEngineer) System.out.println(((SoftwareEngineer) s).getLiabilities());
            else if (s instanceof Lawyer) System.out.println(((Lawyer) s).getLegalActivitiesPlaces());
        });
    }
}
