package com.nick.software.profession.entity;

import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class Manager extends Profession {
    private String title;
    private Map<String, Set<Profession>> projects;
    private Set<Profession> subordinates;
    private Set<String> liabilities;
    private Queue<String> customers;

    public Manager() {
    }

    public Manager(String firstName, String lastName, String middleName,
                   long IIN, Profession previousProfession, String title, Map<String,
                   Set<Profession>> projects, Set<Profession> subordinates, Set<String> liabilities, Queue<String> customers) {
        super(firstName, lastName, middleName, IIN, previousProfession);
        this.title = title;
        this.projects = projects;
        this.subordinates = subordinates;
        this.liabilities = liabilities;
        this.customers = customers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, Set<Profession>> getProjects() {
        return projects;
    }

    public void setProjects(Map<String, Set<Profession>> projects) {
        this.projects = projects;
    }

    public Set<Profession> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(Set<Profession> subordinates) {
        this.subordinates = subordinates;
    }

    public Set<String> getLiabilities() {
        return liabilities;
    }

    public void setLiabilities(Set<String> liabilities) {
        this.liabilities = liabilities;
    }

    public Queue<String> getCustomers() {
        return customers;
    }

    public void setCustomers(Queue<String> customers) {
        this.customers = customers;
    }

    public List<? extends Profession> entireExperience(){
        return super.entireExperience();
    }
}
