package com.nick.software.profession.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Lawyer extends Profession {
    private String education;
    private int numberOfCases;
    private int numberOfWinCases;
    private int experience;
    private Set<String> legalActivitiesPlaces;

    public Lawyer() {
    }

    public Lawyer(String firstName, String lastName, String middleName, long IIN, Profession previousProfession,
                  String education, int numberOfCases, int numberOfWinCases, int experience, Set<String> legalActivitiesPlaces) {
        super(firstName, lastName, middleName, IIN, previousProfession);
        this.education = education;
        this.numberOfCases = numberOfCases;
        this.numberOfWinCases = numberOfWinCases;
        this.experience = experience;
        this.legalActivitiesPlaces = legalActivitiesPlaces;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public int getNumberOfCases() {
        return numberOfCases;
    }

    public void setNumberOfCases(int numberOfCases) {
        this.numberOfCases = numberOfCases;
    }

    public int getNumberOfWinCases() {
        return numberOfWinCases;
    }

    public void setNumberOfWinCases(int numberOfWinCases) {
        this.numberOfWinCases = numberOfWinCases;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public Set<String> getLegalActivitiesPlaces() {
        return legalActivitiesPlaces;
    }

    public void setLegalActivitiesPlaces(Set<String> legalActivitiesPlaces) {
        this.legalActivitiesPlaces = legalActivitiesPlaces;
    }

    public List<? extends  Profession> entireExperience(){
       return super.entireExperience();
    }
}
