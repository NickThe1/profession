package com.nick.software.profession.entity;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Welder extends Profession {
    private static Set<String> welderTypes;
    private String rank;
    private Set<String> detailTypes;
    private String welderType;
    private Map<String, Integer> details;

    static {
        welderTypes.add("Electric welder");
        welderTypes.add("Gas welder");
    }

    public Welder() {
    }

    public Welder(String firstName, String lastName, String middleName,
                  long IIN, Profession previousProfession, String rank, Set<String> detailTypes,
                  String welderType, Map<String, Integer> details) {
        super(firstName, lastName, middleName, IIN, previousProfession);
        this.rank = rank;
        this.detailTypes = detailTypes;
        this.welderType = welderType;
        this.details = details;
    }

    public static Set<String> getWelderTypes() {
        return welderTypes;
    }

    public static void setWelderTypes(Set<String> welderTypes) {
        Welder.welderTypes = welderTypes;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Set<String> getDetailTypes() {
        return detailTypes;
    }

    public void setDetailTypes(Set<String> detailTypes) {
        this.detailTypes = detailTypes;
    }

    public String getWelderType() {
        return welderType;
    }

    public void setWelderType(String welderType) {
        this.welderType = welderType;
    }

    public Map<String, Integer> getDetails() {
        return details;
    }

    public void setDetails(Map<String, Integer> details) {
        this.details = details;
    }

    public List<? extends Profession> entireExperience(){
        return super.entireExperience();
    }
}
