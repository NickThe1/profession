package com.nick.software.profession.entity;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class SoftwareEngineer extends Profession {
    private String title;
    private Set<String> liabilities;
    private Set<String> technologies;
    private Map<String, Set<String>> projects;
    private Set<SoftwareEngineer> subordinates;

    public SoftwareEngineer() {
    }

    public SoftwareEngineer(String firstName, String lastName, String middleName,
                            long IIN, Profession previousProfession, String title,
                            Set<String> liabilities, Set<String> technologies,
                            Map<String, Set<String>> projects, Set<SoftwareEngineer> subordinates) {
        super(firstName, lastName, middleName, IIN, previousProfession);
        this.title = title;
        this.liabilities = liabilities;
        this.technologies = technologies;
        this.projects = projects;
        this.subordinates = subordinates;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<String> getLiabilities() {
        return liabilities;
    }

    public void setLiabilities(Set<String> liabilities) {
        this.liabilities = liabilities;
    }

    public Set<String> getTechnologies() {
        return technologies;
    }

    public void setTechnologies(Set<String> technologies) {
        this.technologies = technologies;
    }

    public Map<String, Set<String>> getProjects() {
        return projects;
    }

    public void setProjects(Map<String, Set<String>> projects) {
        this.projects = projects;
    }

    public Set<SoftwareEngineer> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(Set<SoftwareEngineer> subordinates) {
        this.subordinates = subordinates;
    }

    public List<? extends Profession> entireExperience(){
        return super.entireExperience();
    }
}
