package com.nick.software.profession.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public abstract class Profession {
    private String firstName;
    private String lastName;
    private String middleName;
    private long IIN;
    private Profession previousProfession;

    public Profession() {
    }

    protected Profession(String firstName, String lastName, String middleName, long IIN, Profession previousProfession) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.IIN = IIN;
        this.previousProfession = previousProfession;
    }

    public String getFirstName() {
        return firstName;
    }

    protected void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    protected void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    protected void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public long getIIN() {
        return IIN;
    }

    protected void setIIN(long IIN) {
        this.IIN = IIN;
    }

    public Profession getPreviousProfession() {
        return previousProfession;
    }

    public void setPreviousProfession(Profession previousProfession) {
        this.previousProfession = previousProfession;
    }

    public List<? extends Profession> entireExperience(){
        List<Profession> personProfessions = new ArrayList<>();
        Profession profession = this;

        personProfessions.add(this);
        while (profession.getPreviousProfession() != null){
            personProfessions.add(getPreviousProfession());
            profession = getPreviousProfession();
        }
        return personProfessions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profession that = (Profession) o;
        return IIN == that.IIN &&
                firstName.equals(that.firstName) &&
                lastName.equals(that.lastName) &&
                Objects.equals(middleName, that.middleName) &&
                Objects.equals(previousProfession, that.previousProfession);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, middleName, IIN, previousProfession);
    }
}
