package com.nick.software.profession.entity;

import java.math.BigDecimal;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class Logistician extends Profession {
    private String title;
    private Set<Stack<String>> storage;
    private Set<String> routes;
    private Queue<String> cargo;
    private BigDecimal assets;
    private BigDecimal liabilities;

    public Logistician() {
    }

    public Logistician(String firstName, String lastName, String middleName,
                       long IIN, Profession previousProfession, String title, Set<Stack<String>> storage,
                       Set<String> routes, Queue<String> cargo, BigDecimal assets, BigDecimal liabilities) {
        super(firstName, lastName, middleName, IIN, previousProfession);
        this.title = title;
        this.storage = storage;
        this.routes = routes;
        this.cargo = cargo;
        this.assets = assets;
        this.liabilities = liabilities;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Stack<String>> getStorage() {
        return storage;
    }

    public void setStorage(Set<Stack<String>> storage) {
        this.storage = storage;
    }

    public Set<String> getRoutes() {
        return routes;
    }

    public void setRoutes(Set<String> routes) {
        this.routes = routes;
    }

    public Queue<String> getCargo() {
        return cargo;
    }

    public void setCargo(Queue<String> cargo) {
        this.cargo = cargo;
    }

    public BigDecimal getAssets() {
        return assets;
    }

    public void setAssets(BigDecimal assets) {
        this.assets = assets;
    }

    public BigDecimal getLiabilities() {
        return liabilities;
    }

    public void setLiabilities(BigDecimal liabilities) {
        this.liabilities = liabilities;
    }

    public List<? extends Profession> entireExperience(){
        return super.entireExperience();
    }
}
